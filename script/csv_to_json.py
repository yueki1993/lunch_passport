#coding:utf-8


"""
../Resources/list.csvを読み込んでjsonに変換
"""


import json
import codecs
from collections import defaultdict



"""
[
{
    "shopname":"ヨシミキッチン",
    "food":"ベーコンとキノコのクリームオムライス",
    "limit":None,
    "page":21,
    "open":{
        "monday":[["1100","1600"]],
        "tuesday":[[]],
        ...
        "holiday":[[]]
    },
},
{
    "shopname":""
},...
]
"""

fo = codecs.open("../Resources/list.json", "w", "shift_jis")
J=[]
with codecs.open("../Resources/list.csv", "r", "shift_jis") as fi:
    for line in fi.readlines()[2:80]:
        L = line.split(",")
        shopname = L[0]
        food = L[1]
        limit = L[2] or None
        page = L[3]
        open = defaultdict(list)
        WEEK = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday", "holiday"]
        for w, i in zip(WEEK, range(4, 20, 2)):
            o = L[i].split("-")
            lo = L[i + 1].split("-")
            for oo, loo in zip(o, lo):
                if oo:
                    open[w].append((oo, loo))
                else:
                    open[w]
        d = {
            "shopname": shopname,
            "food": food,
            "limit": limit,
            "page": page,
            "open": open
        }
        J.append(d)
json.dump(J, fo,ensure_ascii=False)