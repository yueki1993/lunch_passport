#coding:utf-8

"""
list.jsonに座標情報を付加
"""

import json
import codecs
import xml.dom.minidom as md
from pprint import pprint

with codecs.open("../Resources/list.json","r","shift-jis") as fi:
    J=json.load(fi)
with codecs.open("../Resources/map.kml") as fi:
    tree=md.parse(fi)

for place in tree.getElementsByTagName("Placemark"):
    shopname = place.getElementsByTagName("name").item(0).firstChild.nodeValue
    coordinates=place.getElementsByTagName("coordinates").item(0).firstChild.nodeValue
    for j in J:
        if j["shopname"] == shopname:
            j["coordinates"]=coordinates
            break
    else:
        print shopname
fo=codecs.open("../Resources/list_with_coordinates.json","w","shift-jis")
#json.dump(J,fo,ensure_ascii=False)
for j in J:
    print >>fo, json.dumps(j,ensure_ascii=False)







