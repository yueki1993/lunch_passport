#coding:utf-8

import json,codecs

with codecs.open("../Resources/list_with_coordinates.json","r","shift-jis") as fi:
    with codecs.open("../Resources/splitted_coordinates.json", "w", "utf-8") as fo:
        for line in fi:
            a = json.loads(line)
            lon,lat=a["coordinates"].split(",")[:2]
            a["coordinates"]={
                "latitude": lat,
                "longitude": lon
            }
            print >>fo, json.dumps(a, ensure_ascii=False)
