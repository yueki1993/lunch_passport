#coding:utf-8

"""
クエリに適合したランチパスポート掲載店一覧を返す
"""
from datetime import datetime, date, time

try:
    import simplejson as json
except ImportError:
    import json
import codecs
import math

from jinja2 import Environment, FileSystemLoader
from lib.jholiday import holiday_name

from constants import KML_TEMPLATE, SHOP_LIST


def _datetime_to_date(datetime_obj):
    return date(year=datetime_obj.year,
                month=datetime_obj.month,
                day=datetime_obj.day)


def _str_to_time(s):
    return time(hour=int(s[0:2]),
                minute=int(s[2:4]))


def _datetime_to_time(dtobj):
    return time(hour=dtobj.hour,
                minute=dtobj.minute)


def _is_open(shop_open_times, query_time):
    """営業時間内か判定する

    Args:
        shop_open_times: stringのリスト
            [[open1, LO1], [open2, LO2], ...]
            example:[["1100", "1400"], ["1800", "2300"]]

        query_time: datetime object

    Returns:
        営業時間内ならTrue, そうじゃなかったらFalse
    """

    return any([_str_to_time(open) <= _datetime_to_time(query_time) <= _str_to_time(last_order)
                for (open, last_order) in shop_open_times])


def _distance(lat1, lon1, lat2, lon2):
    # Hubenyの公式
    # http://d.hatena.ne.jp/c-yan2/20091114/1258176822
    to_rad = lambda deg: deg * math.pi / 180
    lat1, lon1, lat2, lon2 = map(to_rad, (lat1, lon1, lat2, lon2))

    a2 = 6378137.0 ** 2
    b2 = 6356752.314140 ** 2
    e2 = (a2 - b2) / a2
    w = 1 - e2 * math.sin((lat1 + lat2) / 2) ** 2
    c2 = math.cos((lat1 + lat2) / 2) ** 2
    return math.sqrt((b2 / w ** 3) * (lat1 - lat2) ** 2 + (a2 / w) * c2 * (lon1 - lon2) ** 2)


def _get_weekday_info(shopobj, query):
    """
    query["date"]が何曜日なのかを判定して，shopobjの対応する営業時間情報を返す
    """
    is_holiday = bool(holiday_name(date=_datetime_to_date(query["date"])))
    if is_holiday:
        return shopobj["open"]["holiday"]
    else:
        weekday = query["date"].strftime("%A").lower()
        return shopobj["open"][weekday]


def _match_query(shopobj, query):
    """店がqueryを満たしているか判定する

    Args:
        shopobj: 店の情報(dict)
        query: dictionary
            date (datetime object): 必須
            distance (integer): kmで指定 未実装
            categoly (unicode) : 未実装

    Returns:
        もしqueryを満たしてるならTrue, そうでなければFalse
    """

    """
    後々拡張したい…．
    1.半径○km以内
    2.店のカテゴリ
    的なクエリでも受け取れるように
    """

    is_open = _is_open(_get_weekday_info(shopobj, query), query["date"])

    return is_open  #とりあえず


def _add_open_time_info(shopobj, query):
    """
    query["date"]に対応するshopobjの営業時間を別途付与
    """
    shopobj["opening"] = [(open, lo) for (open, lo) in _get_weekday_info(shopobj, query)
                          if _str_to_time(open) <= _datetime_to_time(query["date"]) <= _str_to_time(lo)
    ][0]


def make_map_data(query):
    """クエリに適合した店を列挙する

    dateを満たす店を，coordinatesに近い順に出力する．

    Args:
        query: dictionary
            date: datetimeオブジェクト
            coordinates: (経度, 緯度)のタプル
            後々追加予定
    Returns:
        queryを満たすshopobjのlist
        ただし，現在地に近い順
    """
    with codecs.open(SHOP_LIST, "r", "utf-8") as fi:
        shopdatas_with_dist = []
        for line in fi:
            shopobj = json.loads(line)
            if _match_query(shopobj, query):
                lon1, lat1 = map(float,
                                 (shopobj["coordinates"]["longitude"],
                                  shopobj["coordinates"]["latitude"]))
                shopobj["distance"] = _distance(lat1, lon1, query["coordinates"][0], query["coordinates"][1])
                shopdatas_with_dist.append((shopobj["distance"], shopobj))
                _add_open_time_info(shopobj, query)
    shopdatas_with_dist.sort()

    return [x[1] for x in shopdatas_with_dist]


def make_kml_data(query):
    """クエリに適合したkmlデータを生成する．

    dateを満たす店のkmlデータを，coordinatesに近い順に出力する．

    Args:
        query: dictionary
            date: datetimeオブジェクト
            coordinates: (経度, 緯度)のタプル
            後々追加予定
    Returns:
        kmlデータ(string)
    """
    env = Environment(
        loader=FileSystemLoader(".", encoding='utf-8'),
        autoescape=True)
    template = env.get_template(KML_TEMPLATE)

    return template.render(shopdatas=make_map_data(query))


if __name__ == "__main__":
    #make_kml_data(0,0)

    #test code for jholiday.py
    """
    test_date = []
    test_date.append(date.today())
    test_date.append(_datetime_to_date(datetime.now()))
    test_date.append(date(year=2014,month=7,day=21)) #=>海の日
    for dobj in test_date:
        print bool(holiday_name(date=dobj))
    """

    #test code for _distance
    """
    print _distance(36.10377477777778, 140.08785502777778,
                    -35.65502847222223, 139.74475044444443)
    print _distance(36.10377477777778, 140.08785502777778,
                    35.65502847222223, 139.74475044444443)
    """

    #test code for kml_data
    now = datetime.now()
    coordinates = (140.834842, 38.256739)
    print make_map_data({"date": now, "coordinates": coordinates})

