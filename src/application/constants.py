#coding:utf-8

"""
定数設定ファイル
"""
import os
path = os.path.dirname(os.path.realpath(__file__))

KML_TEMPLATE = "templates/template.kml"
SHOP_LIST = os.path.join(path, "Resources/shoplist.json")