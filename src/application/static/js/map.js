﻿var map;
var infowindows = {};
var markers = {};
var pos = null;

var _direction_service = new google.maps.DirectionsService();
var _direction_display = new google.maps.DirectionsRenderer({ suppressMarkers: true });
var _currentInfoWindow = null;

function set_currentpos_mark() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                pos = [position.coords.latitude,
                    position.coords.longitude];
                initial_mark(pos);
            });
    }
    initial_mark(null);
}

function open_infowindow(infowindow, marker) {
    if (_currentInfoWindow) {
        _currentInfoWindow.close();
    }
    infowindow.open(map, marker);
    _currentInfoWindow = infowindow;

    map.setCenter(marker.getPosition());
    console.log(marker.getPosition());
}



function mark(shop_obj) {
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(shop_obj["coordinates"]["latitude"],
            shop_obj["coordinates"]["longitude"]),
        map: map,
        title: shop_obj["shopname"]
    });
    
    var contentString = "";
    $.get("/infowindow", {
        shopname: shop_obj["shopname"],
        food: shop_obj["food"],
        distance: shop_obj["distance"],
        lat: shop_obj["coordinates"]["latitude"],
        lon: shop_obj["coordinates"]["longitude"],
        limit:shop_obj["limit"],
        page: shop_obj["page"],
        could_get_pos: !!pos
    }, function (data) {
        contentString = data;
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            noSupress: true
        });

        infowindows[shop_obj["shopname"]] = infowindow;
        markers[shop_obj["shopname"]] = marker;

        google.maps.event.addListener(marker, "click", function () {
            open_infowindow(infowindow, marker);
        });
        google.maps.event.addListener(map, "click", function () {
            _currentInfoWindow = null;
            infowindow.close();
        });
    });
}

function go(lat, lon, shopname)
{
    var destination = new google.maps.LatLng(lat, lon);    
    var origin = new google.maps.LatLng(pos[0], pos[1]);
    var request = {
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING
    };
   _direction_service.route(request, function (result, status) {
       if (status == google.maps.DirectionsStatus.OK) {
           _direction_display.setDirections(result);
           console.log(result);
           console.log(infowindows[shopname]);
           //$("#route").html("車で" + result["routes"][0]["legs"][0]["duration"]["text"]+"<br>");
           console.log(result["routes"][0]["legs"][0]["duration"]["text"]);
       }
   });

}


function initial_mark(p) {
    var pos = p || [38.2601997, 140.8821099];
    var now = new Date().getTime() / 1000; // epoch time
    $.get("./api", {
        date: now,
        lat: pos[0],
        lon: pos[1],
    }, function (shop_objs) {
        shop_objs.forEach(mark);
        create_list(shop_objs);
    });
}

function initialize() {
    var stationpos = new google.maps.LatLng(38.2601997, 140.8821099);

    var option = {
        zoom: 15,
        center: stationpos,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), option);
    _direction_display.setMap(map);
    set_currentpos_mark();
    //get_currentposition();
    //initial_mark();

    //disable infowindow of POI
    //http://stackoverflow.com/questions/7950030/can-i-remove-just-the-popup-bubbles-of-pois-in-google-maps-api-v3/19710396#19710396
    (function fixInfoWindow() {
        var set = google.maps.InfoWindow.prototype.set;
        google.maps.InfoWindow.prototype.set = function (key, val) {
            if (key === "map") {
                if (!this.get("noSupress")) {
                    return;
                }
            }
            set.apply(this, arguments);
        }
    })();
}




