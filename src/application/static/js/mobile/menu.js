﻿var pos = null;
var shop_objects = {};

String.prototype.splice = function (idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

function init() {
    map_initialize();
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                pos = [position.coords.latitude,
                    position.coords.longitude];
                get_available_shop(pos);
            });
    }
    get_available_shop(null);
}

function get_available_shop(p) {
    var p = p || [38.2601997, 140.8821099];
    var now = new Date().getTime() / 1000; // epoch time
    //var now = 1406016868.939; //for debug
    $.get("./api", {
        date: now,
        lat: p[0],
        lon: p[1],
    }, function (shop_objs) {
        create_list(shop_objs);
    });
}

function create_list(shop_objs) {
    var content_text = "";
    var p = !!pos;
    console.log(p);
    for (var i = 0; i < shop_objs.length; i++) {
        shop_objects[shop_objs[i]['shopname']] = shop_objs[i];
        if (p) {
            var distance = parseInt(shop_objs[i]["distance"], 10);
            if (distance > 1000) {
                distance = parseInt(distance / 1000, 10).toString() + "km";
            } else {
                distance = distance.toString() + "m";
            }
        }

        var opening = shop_objs[i]["opening"].map(function (s) {
            return s.splice(2, 0, ":")
        }).join("～");

        content_text += "<li>"
            + '<a href="javascript:show(\'' + shop_objs[i]["shopname"] + "'" + ')">'
            + ((p) ? "<p class='ui-li-aside ui-li-desc'>"
            + distance
            + "</p>" : "")
            + "<h3 class='ui-li-heading'>"
            + shop_objs[i]["shopname"]
            + "</h3>"
            + "<p class='ui-li-desc'><strong>"
            + shop_objs[i]["food"]
            + "</strong></p>"
            + (shop_objs[i]["limit"] ? "<p class='ui-li-desc'>"
            + shop_objs[i]["limit"] + "食限定" + "</p>" : "")
            + "<p class='ui-li-desc'>"
            + opening
            + "</p>"
            + "</a></li>";
    }

    update_list(content_text);
}

function update_list(content_text) {
    $("#list").html(content_text);
    $("#list").listview("refresh");
}