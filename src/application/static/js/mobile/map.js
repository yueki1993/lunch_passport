var map;
var _shop_obj;
var _currentInfoWindow = null;
var _current_marker = null;


function getRealContentHeight() {
    //http://stackoverflow.com/questions/16634224/jquery-mobile-google-maps-wrong-height
    var header = $.mobile.activePage.find("div[data-role='header']:visible");
    var footer = $.mobile.activePage.find("div[data-role='footer']:visible");
    var content = $.mobile.activePage.find("div[data-role='content']:visible:visible");
    var viewport_height = $(window).height();

    var content_height = viewport_height - header.outerHeight() - footer.outerHeight();
    if ((content.outerHeight() - header.outerHeight() - footer.outerHeight()) <= viewport_height) {
        content_height -= (content.outerHeight() - content.height());
    }
    return content_height;
}


function map_initialize() {
    var stationpos = new google.maps.LatLng(38.2601997, 140.8821099);

    var option = {
        zoom: 15,
        center: stationpos,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), option);

    var height = getRealContentHeight();
    $("#map_canvas").height(height);

    $('#map').bind('pageshow', function () {
        google.maps.event.trigger(map, 'resize');
        show_mark();
    });


    //disable infowindow of POI
    //http://stackoverflow.com/questions/7950030/can-i-remove-just-the-popup-bubbles-of-pois-in-google-maps-api-v3/19710396#19710396
    (function fixInfoWindow() {
        var set = google.maps.InfoWindow.prototype.set;
        google.maps.InfoWindow.prototype.set = function (key, val) {
            if (key === "map") {
                if (!this.get("noSupress")) {
                    return;
                }
            }
            set.apply(this, arguments);
        }
    })();
}

function open_infowindow(infowindow, marker) {
    if (_currentInfoWindow) {
        _currentInfoWindow.setMap(null);
        _currentInfoWindow = null;
    }
    if (_current_marker) {
        _current_marker.setMap(null);
        _current_marker = null;
    }
    infowindow.open(map, marker);
    console.log(infowindow);
    console.log(marker);
    _currentInfoWindow = infowindow;
    _current_marker = marker;
}



function show_mark() {
    var shop_obj = _shop_obj;
    shop_position = new google.maps.LatLng(shop_obj["coordinates"]["latitude"],
            shop_obj["coordinates"]["longitude"]);

    var marker = new google.maps.Marker({
        position: shop_position,
        map: map,
        title: shop_obj["shopname"]
    });


    $.get("/infowindow", {
        shopname: shop_obj["shopname"],
        food: shop_obj["food"],
        distance: shop_obj["distance"],
        lat: shop_obj["coordinates"]["latitude"],
        lon: shop_obj["coordinates"]["longitude"],
        limit: shop_obj["limit"],
        page: shop_obj["page"],
        could_get_pos: !!pos
    }, function (content) {
        console.log(content);
        var infowindow = new google.maps.InfoWindow({
            content: content,
            noSupress: true
        });
        open_infowindow(infowindow, marker);
    });
}


function show(shopname) {
    window.location.href = '#map';
    _shop_obj = shop_objects[shopname];
}

function go(lat, lon, shopname) {
    var to_latlon = lat + "," + lon;
    var from_latlon = pos.join(",");
    var url = "http://maps.apple.com?q=" + to_latlon + "&saddr=" + from_latlon + "&daddr=" + to_latlon;
    location.href = url;
}