﻿String.prototype.splice = function( idx, rem, s ) {
    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};

function create_list(shop_objs) {
    var list_content = "";
    for (var i = 0; i < shop_objs.length; i++) {
        if(pos){
            var distance = parseInt(shop_objs[i]["distance"] , 10);
            if(distance > 1000){
                distance = parseInt(distance / 1000, 10).toString() + "km";
            }else{
                distance = distance.toString() + "m";
            }
        }

        var opening = shop_objs[i]["opening"].map(function (s) {
            return s.splice(2, 0, ":")
        }).join("～");

        list_content += "<li>" +
                           '<a href="javascript:show_infowindow(\'' + shop_objs[i]["shopname"] + '\')">'
                           + shop_objs[i]["shopname"]
                           + (pos ? " (" + distance + ")" : "") + "<br>"
                           + shop_objs[i]["food"]
                           + (shop_objs[i]["limit"] ? "<br>" + shop_objs[i]["limit"] + "食限定" : "")
                           + "<br>"
                           + opening
                           + ' </a></li>';
    }
    console.log(list_content);
    $("nav#menu ul").html(list_content);
    $(function () {
        $('nav#menu').mmenu({
            classes: 'mm-light'
        });
    });
}

function show_infowindow(shopname)
{
    open_infowindow(infowindows[shopname], markers[shopname]);
}