#coding:utf-8
from flask import (
    Flask,
    request,
    Response,
    render_template
)

from flask.ext.mobility import Mobility

import sys

try:
    import simplejson as json
except ImportError:
    import json
from datetime import datetime, timedelta
import time

from api import make_map_data

app = Flask(__name__)
Mobility(app)
# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app


@app.route('/')
def mainpage():
    return render_template("mobile/mobile.html" if request.MOBILE
                           else "index.html")


@app.route("/api")
def api():
    date = datetime.utcfromtimestamp(float(request.args.get("date"))) + timedelta(hours=9)
    lon = request.args.get("lon")
    lat = request.args.get("lat")

    return Response(response=json.dumps(make_map_data({
        "date": date,
        "coordinates": map(float, (lat, lon)),
    }), ensure_ascii=False),
                    status=200,
                    mimetype="application/json")


@app.route("/infowindow")
def infowindow():
    shopname = request.args.get("shopname")
    food = request.args.get("food")
    distance = float(request.args.get("distance"))
    page = int(request.args.get("page"))
    limit = request.args.get("limit")
    could_get_pos = request.args.get("could_get_pos") == "true"

    if distance >= 1000:
        distance = "%.1fkm" % (distance / 1000)
    else:
        distance = "%dm" % (distance)

    coords = map(float,
                 (request.args.get("lat"), request.args.get("lon")))
    return render_template("infowindow.html",
                           shopname=shopname,
                           food=food,
                           distance=distance,
                           coords=tuple(coords),
                           page=page,
                           limit=limit,
                           could_get_pos=could_get_pos
    )

if __name__ == '__main__':
    import os

    host = os.environ.get('SERVER_HOST', 'localhost')
    try:
        port = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        port = 5555
    app.run(host, port, debug=True)
